package main

import (
	"fmt"
	"reflect"
)

type any interface{}

type Key interface {
	int | string
}

type path []any

type Table map[any]any

type ToS interface {
	string | Table
}

func (t Table) hasKey(k any) bool {
	return t[k] != nil
}

func (t Table) sub(key any) any {
	return 0
}

func (t Table) travel(p path) any {

	pl := len(p)
	it := t
	var rv any
	for k,v := range p {

		if k == pl-1 {
			rv =  it[v]
		} else {
			if !it.hasKey(v) {
				rv = nil
				return rv
			}

			it = it[v].(Table)
		}

	}

	return rv
}

func main() {
	fmt.Print()

	v := make( Table )
	/*
	{
		set : "mock",
		tes : [
			"first",
			"second",
			[
				"kav"
			]
		]
	}
	*/
	v["set"] = "mock"
	v["tes"] = make(Table)
	v["tes"].(Table)[0] = "first"
	v["tes"].(Table)[1] = "second"
	v["tes"].(Table)[2] = make(Table)
	v["tes"].(Table)[2].(Table)[0] = "kav"

	p := path{"tes",2}
	p2 := path{"tes",2,0}

	r := v.travel(p).(Table)
	fmt.Println("r /tes/2 =", r)
	fmt.Println( reflect.TypeOf( r) )

	fmt.Println()

	r2 := v.travel(p2).(string)
	fmt.Println("r2 /tes/2/0 =", r2)
	fmt.Println( reflect.TypeOf( r2) )

}
