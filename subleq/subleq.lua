#!/bin/lua5.4

local memory = { }
local pc = 0
-- if trying to access empty memory get a 0, effectively
-- an 'infinite' address space
memory = setmetatable(memory, {__index = function() return 0 end })

local function output(a)
	io.write(string.char(a))
end
local function input()
	return string.byte(io.read())
end
-- instruction
local function subleq(a,b,c)
	local at_a
	local result
	-- wether a is an address or get input (-1)
	if     a >=  0 then at_a = memory[a+1]
	elseif a == -1 then at_a = input() end
	-- wether a is an address or output
	if b >= 0 then
		result = memory[b+1] - at_a
		memory[b+1] = result
	elseif b == -1 then
		output(at_a)
		result = 0
	end

	if result <= 0 then pc = c
	               else pc = pc + 3 end
end

-- debug suff
local cycle = 0
local function print_mem()
	local mstr = "\n------------------------,\n"
	for k,v in pairs(memory) do
		if k == pc+1 then mstr = mstr .. '[' end
		mstr = mstr .. v
		if k == pc+3 then mstr = mstr .. ']' end
		mstr = mstr .. '\t'
		if k %3 == 0 then
			mstr = mstr .. '| '.. k-3 ..'\n'
		end
	end
	mstr = mstr .. '------------------------\'\nCycle: ' .. cycle
	mstr = mstr .. '\nPtr pos: ' .. pc
	print(mstr)
end

local hello_world = [[
	 12  12   3
	 36  37   6
	 37  12   9
	 37  37  12
	  0  -1  15
	 38  36  18
	 12  12  21
	 53  37  24
	 37  12  27
	 37  37  30
	 36  12  -1
	 37  37   0
	 39   0  -1
	 72 101 108
	108 111  44
	 32  87 111
	114 108 100
	 33  10  53
]]

local function run(prg)
	-- load program into 'memory'
	for str in string.gmatch(prg, "([^%s]+)") do
		table.insert(memory, tonumber(str))
	end
	-- execution loop, while pc doesn't become -1
	while pc >= 0 do
		--print_mem()
		subleq( memory[pc+1], memory[pc+2], memory[pc+3] )
		cycle = cycle + 1
	end
end
run(hello_world)
