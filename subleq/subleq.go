package main
import (
	"fmt"
	"os"
	"io"
	"log"
)

// Get char and store byte value
func input() int {
	var b [1]byte
	if _, err := io.ReadFull(os.Stdin, b[:]); err != nil {
		log.Fatalln("read:", err)
	}
	return int(b[0])
}

// Print value as byte (char)
func output(v int) {
	if _, err := os.Stdout.Write([]byte{byte(v)}); err != nil {
		log.Fatalln("write:", err)
	}}

// For some reason Go doesn't let you have a const array
var HELLO = []int{
	 12,  12,   3,
	 36,  37,   6,
	 37,  12,   9,
	 37,  37,  12,
	  0,  -1,  15,
	 38,  36,  18,
	 12,  12,  21,
	 53,  37,  24,
	 37,  12,  27,
	 37,  37,  30,
	 36,  12,  -1,
	 37,  37,   0,
	 39,   0,  -1,
	 72, 101, 108,
	108, 111,  44,
	 32,  87, 111,
	114, 108, 100,
	 33,  10,  53,
}

type Machine struct {
	memory []int
	m_size int
	pc     int
	Debug  bool
}

// Create a new machine. Preferably memory_size % 3 == 0
func NewMachine(memory_size int) *Machine {
	return &Machine{
		memory: make([]int, memory_size),
		m_size: memory_size,
		pc:     0,
		Debug:  false,
	}}

// 'Load' an array of numbers into the working memory
func (m *Machine) Load(source []int, offset int) {
	var s_len = len(source)

	for i := 0; i < s_len; i += 1 {
		m.memory[i + offset] = source[i]
	}}

// Show (non empty) contents of memory, plus pc position and cycle
func (m Machine) show_memory(cycle int) {
	/*
		,-------------,
		| 123 456 789 | 0
		| 101 112 131 | 3 <--
		 ...
	*/
	fmt.Print("\n,-----------------------,\n")
	for i := 0; i < 252; i += 3 {
		if (m.memory[i] != 0) || (m.memory[i+1] != 0) || (m.memory[i+2] != 0) {
			fmt.Print("| ")
			fmt.Print(m.memory[i],"\t", m.memory[i+1],"\t", m.memory[i+2])
			fmt.Print("\t|", i, )
			if (m.pc == i) || (m.pc == i+1) || (m.pc == i+2) {
				fmt.Print(" <--")
			}
			fmt.Printf("\n")
		}}
	fmt.Print("·-----------------------'\n")
	fmt.Print("| cycle ", cycle, "\tpc ", m.pc, "\n")
}

// SUBtract and branch if Less-than or EQual to zero", returns the next pc position
func (m *Machine) subleq() {
	var (
		a = m.memory[m.pc   ]
		b = m.memory[m.pc +1]
		c = m.memory[m.pc +2]
		result int
	)

	switch {
		case a == -1: m.memory[b] = input()
		case b == -1: output( m.memory[a] )
		default:
			result = m.memory[b] - m.memory[a]
			m.memory[b] = result
			if result <= 0 {
				m.pc = c
				return
			}}
	m.pc += 3
}

func (m Machine) Run() {
	var cycle = 0
	for m.pc >= 0 {
		if m.Debug { m.show_memory(cycle) }
		m.subleq()
		cycle += 1
	}
	if m.Debug { fmt.Print("' Machine halted.\n") }
}

func main() {
	var m = NewMachine(255)
	m.Load(HELLO, 0)
	m.Run()
}
