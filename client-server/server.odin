package main

import "sock"
import "core:c"
import "core:os"
import "core:fmt"
import "core:time"
import "core:thread"

make_byte_array :: inline proc($N: int) -> (res: [N]byte) {
    for _, i in res {
        res[i] = cast(byte)0;
    }
    return;
}

byteArr_to_str::proc(bt:[255]byte) -> string {
	runes : [dynamic]rune;
	stringo : string = "";
	for _,i in bt {
		if bt[i] == cast(byte)10 {
			break;
		}
		append(&runes, cast(rune)bt[i]);
	}
	for i:=0; i < len(runes); i+=1 {
		stringo = fmt.aprintf("%v%v",stringo,runes[i]);
	}

	return stringo;
}

do_things::proc(t: ^thread.Thread){

	// from rawptr back to data
	data := (cast(^ThreadData)t.data)^;

	clients := data.clients;
	client_count := data.client_count;
	client := clients[client_count];

	messages := data.messages;
	messages_write := data.messages_write;

	command := data.command;
	admin_password := data.admin_password;
	admin := false;

	//fmt.printf("--client: %v\n", client);

	os.write_string(cast(os.Handle)client, "Welcome! '.exit' to quit\n")
	
	msg : [255]byte;
	name : string = "";
	os.write_string(cast(os.Handle)client, "Say your name!\n");
	//msg = make_byte_array(255);
	os.write_string(cast(os.Handle)client, "Name> ");	
	os._unix_read(cast(os.Handle)client, &msg, c.size_t(len(msg)) );
	name = byteArr_to_str(msg);
	if (name == "") || (name == " ") {
		os.write_string(cast(os.Handle)client, "Invalid name\n");	
	}
	if !messages_write^ {
		messages_write^ = true;
		append(messages, fmt.aprintf("> %v joined", name) );	
		messages_write^ = false;
	}
	for {

		msg = make_byte_array(255);
		//os.write_string(cast(os.Handle)client, "Mesage> ");
		os._unix_read(cast(os.Handle)client, &msg, c.size_t(len(msg)) );
		
		if byteArr_to_str(msg) == "" {
			continue;
		}	
		if byteArr_to_str(msg) == ".exit" {
			break;
		}	
		if byteArr_to_str(msg) == ".login" {
			os.write_string(cast(os.Handle)client, "Admin password: ");
			os._unix_read(cast(os.Handle)client, &msg, c.size_t(len(msg)) );
			if byteArr_to_str(msg) == admin_password {	
				os.write_string(cast(os.Handle)client, "You are now an admin\n");
				admin = true;
			} else {
				os.write_string(cast(os.Handle)client, "Wrong password\n");
			}
			continue;
		}	
		if (byteArr_to_str(msg) == ".shutdown") && admin {
			command^ = "shutdown";
			continue;
		}	

		//fmt.printf("%v: %v\n", name, byteArr_to_str(msg));
		if !messages_write^ {
			messages_write^ = true;
			append(messages, fmt.aprintf("%v: %v", name, byteArr_to_str(msg)) );	
			messages_write^ = false;
		}
	}
	os.close(cast(os.Handle)client);
	if !messages_write^ {
		messages_write^ = true;
		append(messages, fmt.aprintf("< %v quit", name) );	
		messages_write^ = false;
	}
	clients[client_count] = 0;
}
ThreadData::struct {
	clients : ^[2]os.Handle,
	client_count: int,
	messages : ^[dynamic]string,
	messages_write: ^bool,
	command: ^string,
	admin_password: string,
}

send_messages::proc(t: ^thread.Thread) {	
	data := (cast(^SendData)t.data)^;

	messages := data.messages;
	messages_reading := data.messages_reading;
	clients := data.clients;

	last_m := 0;
	curr_m := 0;
	for {
		last_m = len(messages^);

		if curr_m <= last_m-1 {
			
			fmt.printf("| %v\n", messages[curr_m]);
			for i := 0; i < len(clients^); i +=1{			
				if client := clients[i]; client != 0 {
					os.write_string(cast(os.Handle)clients[i], fmt.aprintf("%v\n",messages[curr_m]));
				}
			}

			curr_m += 1;
		}
	}

}
SendData ::struct {
	messages : ^[dynamic]string,
	messages_reading: ^bool,
	clients: ^[2]os.Handle,
}

listen::proc(t: ^thread.Thread) {	
	data := (cast(^ListenData)t.data)^;

	listenfd := data.listenfd;
	clients := data.clients;
	client_count := data.client_count;
	create := data.new_thread;

	for {
		s := sock.accept(listenfd, nil, 0);
		for i := 0; i < len(clients^); i += 1 {
			if clients[i] == 0 {
				client_count^ = i;
				clients[i] = s;
				break;
			}
		}
		create^ = true;
	}

}
ListenData :: struct{
	listenfd: os.Handle,
	clients: ^[2]os.Handle,
	client_count : ^int,
	new_thread: ^bool,
}

main :: proc() {
	
	port := 8082;

	serv_addr: sock.Addrin;

	listenfd := sock.socket(sock.AddrFamily.INET, sock.Type.STREAM, 0);

	serv_addr.family = cast(c.short) sock.AddrFamily.INET;
	serv_addr.addr.addr = cast(c.ulong) sock.htonl(0);
	serv_addr.port = sock.htons( u16(port) );

	sock.bind(listenfd, &serv_addr, size_of(serv_addr));

	sock.listen(listenfd, 2);
	fmt.printf("Listening on %v\n", port);
	
	clients : [2]os.Handle;
	client_count := 0;
	
	threads : [dynamic]^thread.Thread;

	messages : [dynamic]string;
	messages_write := false;
	messages_reading := false;
	
	new_thread := false;
	
	run := true;

	command := "";

	admin_password := "password";

	if t := thread.create(listen); t != nil {
		t.init_context = context;
		t.user_index = len(threads);
		t.data = &ListenData{listenfd, &clients, &client_count, &new_thread};
		append(&threads, t);
		thread.start(t);
	}
	
	if t := thread.create(send_messages); t != nil {
		t.init_context = context;
		t.user_index = len(threads);
		t.data = &SendData{&messages, &messages_reading, &clients};
		append(&threads, t);
		thread.start(t);
	}

	for run {
		
		if new_thread {
			if t := thread.create(do_things); t != nil {
				t.init_context = context;
				t.user_index = len(threads);
				t.data = &ThreadData{&clients, client_count, &messages, &messages_write, &command, admin_password};
				append(&threads, t);
				thread.start(t);
			}
			new_thread = false;
		}

		switch command {
			case "shutdown": {run = false; break;}
			case: {}
		}
		command = "";


		for i := 0; i < len(threads); i += 1 {
			if t := threads[i]; thread.is_done(t) {
				//fmt.printf("Thread is done\n");
				thread.destroy(t);
				ordered_remove(&threads, i);
			}
		}
		time.sleep(750);
	}

	fmt.printf("Server closing\n");
	for i := 0; i < len(threads); i += 1 {
		if t := threads[i]; thread.is_done(t) {
			//fmt.printf("Thread is done\n");
			thread.destroy(t);
			ordered_remove(&threads, i);
		}
	}
	//delete(threads);
}

