// It's only exact palindromes because it also counts spaces and marks
fn is_perf_plaindrome(ist: &str) -> bool {
    let stv:Vec<char> = ist.chars().collect(); let lst = stv.len();
    for i in 0..(lst/2) {
        if stv[i] != stv[lst - i - 1]{ return false; }
    }
    true
}
// Choose between two strings by a bool
trait Fms { fn fmt_bool<'a>(self, tro:&'a str, fao:&'a str) -> &'a str; }
impl Fms for bool {
    fn fmt_bool<'a>(self, tru:&'a str, fal:&'a str) -> &'a str {
        match self { true => tru, false => fal, }
    }
}

fn main(){
    use std::{io::Write, env};
    let mut stt:String = String::new();
    {
        // Use first argument, if no first argument, ask for text
        let args: Vec<_> = env::args().collect();

        if args.len() > 1 { stt = String::from(&args[1]); }
        else {
            print!("Input string -> "); std::io::stdout().flush().expect("");
            std::io::stdin().read_line(&mut stt).expect("");
            stt = String::from(stt.trim());
        }
    }
    println!("\"{}\" {} an exact palindrome", stt, is_perf_plaindrome(&stt).fmt_bool("is", "isn't"));
}
